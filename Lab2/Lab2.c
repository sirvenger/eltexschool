
//Вариант 6
//Расположить строки по убыванию количества слов 
//Входные параметры:
//1. Массив 
//2. Размерность массива 
//Выходные параметры:
//1. Количество перестановок 
//2. Максимальное количество слов
//Прототип функции для ввода строк 
//length = inp_str(char* string, int maxlen); 
// length – длина строки 
// string – введенная строка 
// maxlen – максимально возможная длина строки (размерность массива string) 

//Прототип функции для вывода строк 
//void out_str(char* string, int length, int number);
// string – выводимая строка 
// length – длина строки 
// number – номер строки 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define maxlen 1024

//struct superMas
//{
//	char **mas = Null;
//	int len = 0;
//};

char ** readMas(int count);
void sort(char **mas, int count);
void outMas(char **mas, int count);
void freeMas(char **mas, int count);
int checkword(char *mas);

int main(int argc, char const *argv[])
{
	int count;
	char **masstr;
	printf("Введите количество строк\n");
	scanf("%d\n",&count);
	masstr = readMas(count);
	sort(masstr,count);
	printf("Вывод отсортированного массива\n");
	outMas(masstr,count);
	free(masstr);
	//freeMas(masstr,count);
	return 0;
}

char** readMas(int count){
	char buffer[maxlen];
	char **mas;
	mas = (char** )malloc(sizeof(char*)*count);
	for (int i = 0; i < count; ++i)
	{
		gets(buffer);
		mas[i] = (char*)malloc(sizeof(char)*strlen(buffer));
		strcpy(mas[i],buffer);
	}
	return mas;
}

int checkword(char *mas){
	int i = 1;
 	while (*mas++){
 		char c = *mas;
 		if ( c == ' ') i++;	
 	}
 	return i;
}

void sort (char **mas, int count){
	int quanword1 = 0, quanword2 = 0;
	for (int i = 0; i < count - 1; ++i)
	{
		for (int j = 0; j < count - i - 1; ++j)
		{
			quanword1 = checkword(mas[j]);
			quanword2 = checkword(mas[j+1]);
			//count<<quanword1<<":"<<quanword2;
			if (quanword1 < quanword2) {
				char buffer[maxlen];
				strcpy(buffer,mas[j]);
				strcpy(mas[j], mas[j+1]);
				strcpy (mas[j+1], buffer);
			} 
		}
	}
}

void outMas(char **mas, int count){
    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]);
    }
}

//void freeMas(char **mas, int count){/
//	for (int i = 0; i < count; i++){
//        free(mas[i]); // освобождаем память для отдельной строки
//    }
//    free(mas); // освобождаем памать для массива указателей на строки
//}
