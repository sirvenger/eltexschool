/*4. Warcraft. Заданное количество юнитов добывают золото равными 
орциями из одной шахты, задерживаясь в пути на случайное время, 
о ее истощения. Работа каждого юнита реализуется в порожденном 
роцессе.*/
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/file.h>

#define MAX_UNIT 100
#define MAX_MINEGOLD 100000
//сколько юнит заберает золота
#define GET_GOLD 20

int mineGold(int numberUnit, int gold){
	FILE *fp;
	int fd;
	while (gold > 0){
		sleep(rand()%4);
		fp=fopen("maxMineGold","r");
		fd = fileno(fp);
		flock(fd, LOCK_EX);
		fscanf(fp,"%d",&gold);
		flock(fd,LOCK_UN);
		fclose(fp);
		fp=fopen("maxMineGold","w");
		fd = fileno(fp);
		if (gold > 0) gold -= GET_GOLD; 
		flock(fd, LOCK_EX);
		fprintf(fp,"%d",gold);
		printf("Юнит %d отнёс золото %d GET_GOLD\n", numberUnit);
		printf("Осталось золота в руднике%d\n", gold);
		flock(fd,LOCK_UN);
		fclose(fp);
	}
	return numberUnit;
}

int main(int argc, char** argv) {
	FILE *fp;
	int units,pid[MAX_UNIT],status,stat,i=0,gold;
	if (argc < 2){ 
		printf("Не введено количество юнитов\n"); 
		exit(-1);
	}
	units = atoi(argv[1]);
	fp=fopen("maxMineGold","r");
	fscanf(fp,"%d",&gold);
	fclose(fp);
	for (int i = 0; i < units; ++i)
	{
		pid[i] = fork();
		srand(getpid());
		if (-1 == pid[i]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        } else if (0 == pid[i]) {
            printf(" CHILD: Это %d процесс-потомок СТАРТ!\n", i);
            //sleep(rand() % 4);
            printf(" CHILD: Это %d процесс-потомок ВЫХОД!\n", i);
            
            exit(mineGold(i,gold)); /* выход из процесс-потомока */
        }
	}
	printf("PARENT: Это процесс-родитель!\n");
    // ожидание окончания выполнения всех запущенных процессов
    for (i = 0; i < units; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
        	// printf("Золото в руднике %d\n", maxMineGold);
        }
    }
    return 0;	
}