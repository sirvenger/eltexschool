#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define ECHOMAX 255     /* Longest string to echo */
#define T 10

void DieWithError(char *errorMessage);  /* External error handling function */

int main(int argc, char *argv[])
{
    int sock, tcpSock;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    struct sockaddr_in fromAddr;     /* Source address of echo */
    struct sockaddr_in addr;
    unsigned short echoServPort;     /* Echo server port */
    unsigned int fromSize;           /* In-out of address size for recvfrom() */
    char *servIP;                    /* IP address of server */
    char *echoString;                /* String to send to echo server */
    char message[1024];
    char echoBuffer[ECHOMAX+1], answerBuffer[] = "requsition";      /* Buffer for receiving echoed string */
    int echoStringLen;               /* Length of string to echo */
    int respStringLen, count = 120;               /* Length of received response */

    for (;;)
    {
        if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
            DieWithError("socket() failed");
        memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
        echoServAddr.sin_family = AF_INET;                /* Internet address family */
        echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
        echoServAddr.sin_port = htons(4951); 

        if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        DieWithError("bind() failed");
    

        fromSize = sizeof(fromAddr);
        recvfrom(sock, echoBuffer, ECHOMAX, 0, (struct sockaddr *) &fromAddr, &fromSize);

        if (echoBuffer[0] == answerBuffer[0]){
            tcpSock = socket(AF_INET, SOCK_STREAM, 0);
            if (tcpSock < 0) 
            {
                perror("socket");
                exit(1);
            }
            addr.sin_family = AF_INET;
            addr.sin_port = htons(3900); // или любой другой порт...
            addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);   
            if(connect(tcpSock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
            {
                perror("connect");
                exit(2);
            }
            strcpy(message,"requsition accept");
            printf("Есть соединение\n");
            send(tcpSock,message,sizeof(message),0); 
            char *text = (char*)malloc(sizeof(char) * 1024);
            recv(tcpSock,text,sizeof(char) * 1024,0); 
            printf("message recv %s\n",text);
            close(tcpSock);
        }
        
        printf("Received: %s\n", echoBuffer);    /* Print the echoed arg */
        close(sock);
        sleep(T);
    }
    exit(0);
}
