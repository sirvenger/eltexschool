#include <sys/msg.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <string.h>
#include <wait.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <algorithm>
#include <set>

#define MAX_MSG 1024
#define N 10
#define K 20
#define PORT_UDPI 4950
#define PORT_UDPR 4951
#define PORT_TCP 3900
#define MAX_SEND_SIZE 1024

using namespace std;

int msgqid, qtype = 1;
struct msqid_ds info;
char buf[MAX_SEND_SIZE],sender[MAX_SEND_SIZE];

struct mymsgbuf
{
	int mtype;
	char mtext[MAX_SEND_SIZE];	
} qbuf;

void send_msg(int qid, struct mymsgbuf *qbuf, long type, char *text);
void recv_msg(int qid, struct mymsgbuf *qbuf, long type, char *text);
void DieWithError(char *errorMessage);
int pmsg_full();
int pmsg_empty();

int main(int argc, char const *argv[])
{
    int listener , listener1;
    struct sockaddr_in addr, addr1;
    int i,pid[1],bytes_read,qtype = 1;
	key_t key = 10;

    /*if (argc != 2){
    	fprintf(stderr,"Usage:  %s <SERVER PORT>\n", argv[0]);
        exit(1);
    }
    */
    if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
        perror("msgget");
        exit(1);
    }
    //msgctl(msgqid, IPC_RMID, NULL);
    //printf("Chekc1\n");
    for (i = 0; i < 2; i++){
    	pid[i] = fork();
    	if ( pid[i] == - 1){
    		perror("fork");
    		exit(1);
    	} else if (pid[i] == 0){
    		printf("CHILD START: %d\n",i);
    		if (i == 0) exit(pmsg_full());
    		if (i == 1) exit(pmsg_empty());
    	}
    }

    printf("PARENT\n");

    listener = socket(AF_INET, SOCK_STREAM, 0);
    //listener1 = socket(AF_INET, SOCK_STREAM, 0);
    
    if(listener < 0 )//|| listener1 < 0)
    {
        perror("socket");
        exit(1);
    }
    
    fcntl(listener, F_SETFL, O_NONBLOCK);
    //fcntl(listener1, F_SETFL, O_NONBLOCK);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT_TCP);
    addr.sin_addr.s_addr = INADDR_ANY;

    // addr1.sin_family = AF_INET;
    // addr1.sin_port = htons(3901);
    // addr1.sin_addr.s_addr = INADDR_ANY;


    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0) 
        //|| bind(listener1, (struct sockaddr *)&addr1, sizeof(addr1)) < 0)
    {
       perror("bind");
       exit(2);
    }

    listen(listener, 2);
    //listen(listener1, 2);

    set<int> clients;
    clients.clear();

    while(1)
    {
        // Заполняем множество сокетов
        fd_set readset;
        FD_ZERO(&readset); //Очищает множество set
        FD_SET(listener, &readset); //Добавляет дискриптор listener множетсво set

        for(set<int>::iterator it = clients.begin(); it != clients.end(); it++)
            FD_SET(*it, &readset);

        // Задаём таймаут
        timeval timeout;
        timeout.tv_sec = 300;
        timeout.tv_usec = 0;	

        // Ждём события в одном из сокетов
        int mx = max(listener, *max_element(clients.begin(), clients.end()));
        if(select(mx+1, &readset, NULL, NULL, &timeout) <= 0)
        {
            perror("select");
            exit(3);
        }
        
        // Определяем тип события и выполняем соответствующие действия
        if(FD_ISSET(listener, &readset))
        {
            // Поступил новый запрос на соединение, используем accept
            int sock = accept(listener, NULL, NULL);
            if(sock < 0)
            {
                perror("accept");
                exit(3);
            }
            
            fcntl(sock, F_SETFL, O_NONBLOCK);

            clients.insert(sock);
        }

        for(set<int>::iterator it = clients.begin(); it != clients.end(); it++)
        {
            if(FD_ISSET(*it, &readset))
            {
                // Поступили данные от клиента, читаем их
                //buf = (char*)malloc(sizeof(char) * 1024); 
                bytes_read = recv(*it, buf, MAX_SEND_SIZE, 0);

                if(bytes_read <= 0)
                {
                    // Соединение разорвано, удаляем сокет из множества
                    close(*it);
                    clients.erase(*it);
                    continue;
                }

                if (buf[0] == 'i')
                send_msg(msgqid, (struct mymsgbuf *)&qbuf, qtype, buf);

                printf("PARENT: %s\n", buf);

                if (buf[0] == 'r')
                {
                    recv_msg(msgqid, &qbuf, qtype, sender);
                    send(*it, sender, MAX_SEND_SIZE,0);
                }
                
                close(*it);
                clients.erase(*it);
            }
        }
    }
    
    return 0;
}


void send_msg(int qid, struct mymsgbuf *qbuf, long type, char *text){
	qbuf->mtype = type;
    strcpy(qbuf->mtext, text);
    printf("SEND_MSG: %s\n", text);
    if ((msgsnd(qid,(struct mymsgbuf *) qbuf, strlen(qbuf->mtext) + 1,0)) == -1){
		perror("msgsnd");
		exit(1);
	}
}

void recv_msg(int qid, struct mymsgbuf *qbuf, long type, char * text){  
	int rc;
    qbuf->mtype = type;
    msgctl(msgqid, IPC_STAT, &info);
    printf("Size qnum %ld\n",(ulong)info.msg_qnum);
	rc = msgrcv(qid, (struct msgbuf *)qbuf, 80, 0, 0);
    printf("RECV_MSG: %s\n",qbuf->mtext);
    if ( rc < 0 ) {
        perror( strerror(errno) );
        printf("msgrcv failed, rc=%d\n", rc);
    }
    text = qbuf->mtext;
}

int pmsg_full(){
	int sock,cliAddrLen;
	struct sockaddr_in echoServAddr;
	struct sockaddr_in echoClntAddr;
	char echoBuffer[] = "Deque not full";
	while (1){
		msgctl(msgqid, IPC_STAT, &info);
		if ((ulong)info.msg_qnum < N){
			if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
				DieWithError("socket failed");
    		
    		echoClntAddr.sin_family = AF_INET;
    		echoClntAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    		echoClntAddr.sin_port = htons(PORT_UDPI);

        	cliAddrLen = sizeof(echoClntAddr);

        	sendto(sock, echoBuffer, strlen(echoBuffer), 0, 
             (struct sockaddr *) &echoClntAddr, cliAddrLen);
        	close(sock);
		} else return 1;
		printf("UDP(1)Отправил сообщение: очередь не пуста\n");
		sleep(K);
	   }
	return 1;
}// Протокол UDP рассылки о пустой очереди

int pmsg_empty()
{
	int sock,cliAddrLen;
    struct sockaddr_in echoServAddr;
    struct sockaddr_in echoClntAddr;
    char echoBuffer[] = "requsition";
    int n = 0;
    while (1){
        msgctl(msgqid, IPC_STAT, &info);
        if ((ulong)info.msg_qnum >= 1){
            if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
                DieWithError("socket failed");
            
            echoClntAddr.sin_family = AF_INET;
            echoClntAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
            echoClntAddr.sin_port = htons(PORT_UDPR);

            cliAddrLen = sizeof(echoClntAddr);

            sendto(sock, echoBuffer, strlen(echoBuffer), 0, 
             (struct sockaddr *) &echoClntAddr, cliAddrLen);
            close(sock);
            n++;
            printf("UDP(2)Отправил сообщение: пришла заявка\n");
        } else if (n == 15) return 0;
        sleep(K);
       }
    return 1;
}// Протокол UDP рассылки прихода заявок 

void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}
