#include <stdio.h>
#include <dlfcn.h>
#define MAX_LEN_STR 80

int main(int argc, char const *argv[])
{
	int (*func)(int,int); //понять что за конструкция??
	void *library_handler;
	int result,a,b;
	library_handler = dlopen("/home/venger/eltex/c_c++/Lab5/libmy1.so",RTLD_LAZY);
	if (!library_handler){
		//если ошибка, то вывести ее на экран
		fprintf(stderr,"dlopen() error: %s\n", dlerror());
		exit(1); // в случае ошибки можно, например, закончить работу программы
	};

	func = dlsym( library_handler, "mul" );
	a = 5; b = 6;
	result = (*func)(a,b);
	printf("Умножение %d\n", result);
	func = dlsym( library_handler, "div" );
	a = 30; b = 6;
	result = (*func)(a,b);
	printf("Деление %d\n", result);
	dlclose( library_handler );
	return 0;
}
