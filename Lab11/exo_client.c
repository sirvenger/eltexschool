#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#define MAX_ANS 1024
#define LEFT 1
#define RIGHT 2
#define UP 3
#define DOWN 4

char *message;
char buf[MAX_ANS];

int main()
{
    int n,m,x,y,planes,max = 0;
    int sock,side,quan;
    struct sockaddr_in addr;
    int count = 80;
    message = (char*)malloc(sizeof(char) * count);

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0)
    {
        perror("socket");
        exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(10425); // или любой другой порт...
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("connect");
        exit(2);
    }
    message = "matr";
    send(sock,message,sizeof(message),0);
    recv(sock, buf, 1024, 0);
    
    n = 5; m = 5;
    printf("Введите координаты самолёта\n");
    scanf("%d%d",&x,&y);
    quan = 0;
    max = m - y; side = RIGHT;
    if (y + 1 > max) { max = y + 1; side = LEFT; }
    if (n - x  > max) { max = n - x; side = DOWN; }
    if (x + 1 > max) { max = x + 1; side = UP; }
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            printf("%c",buf[i*n+j]);
        }
        printf("\n");
    }
        
    for (int i = 0; i < max; ++i)
    {
        if (buf[x*n+y-i] == '1' && side == LEFT) quan++;
        if (buf[x*n+y+i] == '1' && side == RIGHT) quan++;
        if (buf[x*n+y-i*n] == '1' && side == UP) quan++;
        if (buf[x*n+y+i*n] == '1' && side == DOWN) quan++;       
    }
    
    char mes[80];
    sprintf(mes,"%d",quan);  
    send(sock,mes,sizeof(mes),0);

    close(sock);

    return 0;
}   