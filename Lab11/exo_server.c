#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#define MAX_SIZE 32

char *create_matr(int n, int m){
    char *matr;
    matr = (char *)malloc(sizeof(char)*MAX_SIZE*MAX_SIZE);
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            matr[i*n+j] = rand()%2 + '0';
        }
    }
    return matr;
}

int main()
{
    int sock, listener;
    struct sockaddr_in addr;
    char buf[1024];
    char *rcvmessage, *matr;
    int bytes_read;
    matr = (char *)malloc(sizeof(char)*MAX_SIZE*MAX_SIZE);

    matr = create_matr(5,5);

    listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0)
    {
        perror("socket");
        exit(1);
    }
    
    addr.sin_family = AF_INET;
    addr.sin_port = htons(10425);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }

    listen(listener, 1);


    while(1)
    {
        sock = accept(listener, NULL, NULL);
        if(sock < 0)
        {
            perror("accept");
            exit(3);
        }

        while(1)
        {
            bytes_read = recv(sock, buf, 1024, 0);
            if(bytes_read <= 0) break;
            //printf("%s\n",matr);
            if ( buf[0] == 'm' && buf[1] == 'a' && buf[2] == 't' && buf[3] == 'r') 
                send(sock, matr, sizeof(matr)*MAX_SIZE*MAX_SIZE, 0);
            else 
            printf("Найдено самолетом целей: %s\n",buf);        
        }
    
        close(sock);
    }
    
    return 0;
}