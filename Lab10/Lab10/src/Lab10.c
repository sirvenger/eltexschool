/*
 ============================================================================
 Name        : Lab10.c
 Author      : Askhat
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */
// 5. Винни-Пух и пчелы. Заданное количество пчел добывают
// мед равными порциями, задерживаясь в пути на случайное
// время. Винни-Пух потребляет мед порциями заданной
// величины за заданное время и столько же времени может
// прожить без питания.
// Работа каждой пчелы реализуется в порожденном процессе.


#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#define GET_HONEY 10
#define EAT_HONEY 10
#define EAT_TIME 2
#define MAX_BEES 100
#define MISKA 1
#define MAX_HONEY 500
#define FALSE 0
#define TRUE 1

struct {
	pthread_mutex_t	mutex;
	int	buff;
} shared = {
	PTHREAD_MUTEX_INITIALIZER
};

void *produce(void *), *eating(void *);
int main(int argc, char const *argv[])
{
	int i,bees,count[MAX_BEES+1];
	pthread_t tid_produce[MAX_BEES+1];
	/*if (argc < 2){
		printf("Не введино количество пчёл\n");
		exit(-1);
	}*/
	printf("Введите количество пчёл:");
	scanf("%d",&bees);

	for (i = 0; i < bees + MISKA; i++ ){
		count[i] = 0;
		if (i + MISKA != bees + MISKA) pthread_create(&tid_produce[i],NULL,produce,&count[i]);
		else
		pthread_create(&tid_produce[i],NULL,eating,&count[i]);
	}

	for (i = 0; i < bees + MISKA; i++) {
			pthread_join(tid_produce[i], NULL);
			printf("Producer #%lu makes %d elements\n", tid_produce[i], count[i]);
	}

	printf("done");
	return 0;
}

void *produce(void *arg){
	for (;;) {
			sleep(rand()%4);
			pthread_mutex_lock(&shared.mutex);
			if (shared.buff > MAX_HONEY) {
				pthread_mutex_unlock(&shared.mutex);
				return(NULL);
			}
			shared.buff += GET_HONEY;
			printf("Произведено 10 мёда\n");
			pthread_mutex_unlock(&shared.mutex);
		}
}

void *eating(void *arg){
	int dead = FALSE;
	for (;;){
		sleep(EAT_TIME);
		pthread_mutex_lock(&shared.mutex);
		if (shared.buff > EAT_HONEY) shared.buff -= GET_HONEY;
		else
		{
			pthread_mutex_unlock(&shared.mutex);
			sleep(EAT_TIME);
			pthread_mutex_lock(&shared.mutex);
			if (shared.buff < EAT_HONEY) dead = TRUE;
			else shared.buff -= GET_HONEY;
		}
		if (dead == TRUE) printf("Медведь умер\n");
		else
		printf("Медведь съел 10 единиц мёда\n");
		printf("Осталось мёда%d\n", shared.buff);
		pthread_mutex_unlock(&shared.mutex);
	}
}
