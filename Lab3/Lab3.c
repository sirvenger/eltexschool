//Название команды 
//Игры 
//Очки 
//Сумма призового фонда 
//Расположить записи в порядке возрастания по сумме призового фонда 

#include <stdio.h>
#include <stdlib.h>
#define maxlen 256

struct team
{
	char name[maxlen];
	int game,point,sum;
};

typedef struct team teams;

void readTeam(teams *tm,FILE *file){
	//printf("Введите Название\n");
	fscanf(file,"%s%d%d%d",tm->name,&tm->game,&tm->point,&tm->sum);
	//printf("Количество игр\n");
	//scanf("%d",&tm->game);
	//printf("Очки\n");
	//scanf("%d",&tm->point);
	//printf("Призовой фонд\n");
	//scanf("%d",&tm->sum);
}

static int  cmp(const void *p1, const void *p2){
	teams *tm1 = *(teams**)p1;
	teams *tm2 = *(teams**)p2;
	return tm2->sum - tm1->sum;
}

void printTeam(teams *tm){
	printf("Название команды %s\n", tm->name );	
	printf("Количество игр %d\n", tm->game );
	printf("Очки %d\n", tm->point );
	printf("Призовой фонд %d\n", tm->sum );
}

int main(int argc, char const *argv[])
{
	int count;
	FILE *file;
	//printf("Введите количество команд\n");
	teams** tm = (teams**)malloc(sizeof(teams**)*count);
	if ((file=fopen("input.txt","r")) == NULL)
	{
		printf("Невозможно открыть файл\n");
		exit(1);
	}
	fscanf(file,"%d", &count);
	for (int i = 0; i < count; ++i)
	{
		tm[i] = (teams*)malloc(sizeof(teams));
		readTeam(tm[i],file);
		/* code */
	}
	qsort(tm,count,sizeof(teams*),cmp);
	for (int i = 0; i < count; ++i)
	{
		printf("Команда номер %d\n",i );
		printTeam(tm[i]);
		/* code */
	}
	return 0;
}