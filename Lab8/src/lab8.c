/*
 ============================================================================
 Name        : lab8.c
 Author      : Askhat
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */
// 5. Винни-Пух и пчелы. Заданное количество пчел добывают
// мед равными порциями, задерживаясь в пути на случайное
// время. Винни-Пух потребляет мед порциями заданной
// величины за заданное время и столько же времени может
// прожить без питания.
// Работа каждой пчелы реализуется в порожденном процессе.

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>

#define GET_HONEY 10
#define EAT_HONEY 10
#define EAT_TIME 2
#define MAX_BEES 100
#define MISKA 1
#define MAX_HONEY 1000
#define FALSE 0
#define TRUE 1
#define MAX_SEND_SIZE 80

struct mymsgbuf {
        long mtype;
        char mtext[MAX_SEND_SIZE];
};

int msgqid, rc;
struct mymsgbuf qbuf;
int qtype = 1;

void send_msg(int qid, struct mymsgbuf *qbuf, long type, char *text ){
	qbuf->mtype = type;
	strcpy(qbuf->mtext,text);
	if((msgsnd(qid, (struct msgbuf *)qbuf,strlen(qbuf->mtext)+1, 0)) ==-1)
	{
		perror("msgsnd");
	    exit(1);
    }
}

int rcv_msg(int qid, struct mymsgbuf *qbuf, long type){
	int arg;
	qbuf->mtype = type;
	msgrcv(qid, (struct mymsgbuf*)qbuf, MAX_SEND_SIZE,type,0);
	arg = atoi(qbuf->mtext);
	return arg;
}

int mineHoney(int bee){
//	FILE *fp;
	int honey = 0;
	char str[MAX_SEND_SIZE];
	while (honey <= MAX_HONEY){
		sleep(rand()%5);
		honey = rcv_msg(msgqid,&qbuf,qtype);
		honey += GET_HONEY;
		sprintf(str,"%d",honey);
		send_msg(msgqid,(struct mymsgbuf *)&qbuf,qtype,str);
		printf("Пчела сотворила мёд в количесте %d единиц \n", GET_HONEY );
		printf("Количесвто мёда %d \n", honey );
	}
	return bee;
}

int mishkaEat(){
//	FILE *fp;
	int honey;
	char str[MAX_SEND_SIZE];
	int miskaDead = FALSE;
	while (miskaDead == FALSE){
		sleep(EAT_TIME);
		honey = rcv_msg(msgqid,&qbuf,qtype);
		if ( honey >= EAT_HONEY) honey -= EAT_HONEY;
		else
		{
			send_msg(msgqid,(struct mymsgbuf *)&qbuf,qtype,str);
			sleep(EAT_TIME);
			honey = rcv_msg(msgqid,&qbuf,qtype);
			printf("Количесвто мёда %d \n", honey );
			if ( honey < EAT_HONEY) miskaDead = TRUE;
			if (miskaDead == TRUE) printf("Медведь сдох\n");
			else
			honey -= EAT_HONEY;
		}
		printf("Медведь съел %d единиц мёда\n",EAT_HONEY);
		sprintf(str,"%d",honey);
		send_msg(msgqid,(struct mymsgbuf *)&qbuf,qtype,str);
		//fflush(stdout);
		printf("Количесвто мёда %d \n", honey );
	}
	return 0;
}

int main(int argc, char const *argv[])
{
	struct mymsgbuf qbuf;
	char str [MAX_SEND_SIZE];
	int bees,pid[MAX_BEES],status,stat;
	key_t key;


	key = ftok(".", 'm');
	if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
		perror("msgget");
		exit(1);
	}


	str[0] = '0';
	send_msg(msgqid,(struct mymsgbuf *)&qbuf,qtype,str);

	printf("Введите количесвто пчёл \n");
	scanf("%d",&bees);

	for (int i = 0; i < bees + MISKA; ++i)
	{
		pid[i] = fork();
		srand(getpid());
		if (-1 == pid[i] ){
			perror("fork");
			exit(1);
		} else if (0 == pid[i]){
			if (i == bees){
				printf("Процесс медведь\n");
				exit(mishkaEat());
			} else
			{
				printf("Процесс пчела %d\n", i);
				exit(mineHoney(i));
			}
		}
	}
	printf("PARENT: Это процесс-родитель!\n");
    for (int i = 0; i < bees + MISKA; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
        }
    }

    if ((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0) {
    		perror( strerror(errno) );
    		printf("msgctl (return queue) failed, rc=%d\n", rc);
    		return 1;
    	}

    printf("done\n");
    return 0;
}
