/*
 ============================================================================
 Name        : lab9.c
 Author      : Askhat
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

// 5. Винни-Пух и пчелы. Заданное количество пчел добывают
// мед равными порциями, задерживаясь в пути на случайное
// время. Винни-Пух потребляет мед порциями заданной
// величины за заданное время и столько же времени может
// прожить без питания.
// Работа каждой пчелы реализуется в порожденном процессе.

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <ctype.h>
#include <ctype.h>


#define GET_HONEY 10
#define EAT_HONEY 10
#define EAT_TIME 2
#define MAX_BEES 100
#define MISKA 1
#define MAX_HONEY 1000
#define FALSE 0
#define TRUE 1
#define MAX_SEND_SIZE 80

union semun {
 int val;                  /* значение для SETVAL */
 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
 unsigned short *array;    /* массивы для GETALL, SETALL */
                           /* часть, особенная для Linux: */
 struct seminfo *__buf;    /* буфер для IPC_INFO */
};

int shmid;
key_t key = 69;
int *shm;
int semid;
union semun arg;
struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса

int mineHoney(int bee){
//	FILE *fp;
	if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
			perror("shmat");
			exit(1);
		}
	while ((*shm) <= MAX_HONEY){
		sleep(rand()%5);
		/* Получим доступ к разделяемой памяти */
		if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
			perror("shmat");
			exit(1);
		}
		/* Заблокируем разделяемую память */
		if((semop(semid, &lock_res, 1)) == -1){
			fprintf(stderr, "Lock failed\n");
			exit(1);
		} else{
			printf("Semaphore resources decremented by one (locked) \n");
		}
		(*shm) = (*shm) + GET_HONEY;
		/* Освободим разделяемую память */
		if((semop(semid, &rel_res, 1)) == -1){
			fprintf(stderr, "Unlock failed\n");
			exit(1);
		} else{
			 printf("Semaphore resources incremented by one (unlocked) \n");
		}
		printf("Пчела сотворила мёд в количесте %d единиц \n", GET_HONEY );
		printf("Количесвто мёда %d \n", (*shm) );
	}
	 /* Отключимся от разделяемой памяти */
	if (shmdt(shm) < 0) {
		printf("Ошибка отключения\n");
		exit(1);
	}

	return bee;
}

int mishkaEat(){
//	FILE *fp;
	int miskaDead = FALSE;
	while (miskaDead == FALSE){
		sleep(EAT_TIME);
		/* Получим доступ к разделяемой памяти */
		if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
			perror("shmat");
			exit(1);
		}

		/* Заблокируем разделяемую память */
		if((semop(semid, &lock_res, 1)) == -1){
			fprintf(stderr, "Lock failed\n");
			exit(1);
		} else{
			printf("Semaphore resources decremented by one (locked) \n");
		}

		if ( (*shm) >= EAT_HONEY) (*shm) = (*shm) - EAT_HONEY;
		else
		{
			/* Освободим разделяемую память */
			if((semop(semid, &rel_res, 1)) == -1){
				fprintf(stderr, "Unlock failed\n");
				exit(1);
			} else{
				 printf("Semaphore resources incremented by one (unlocked) \n");
			}
			sleep(EAT_TIME);
			/* Получим доступ к разделяемой памяти */
			if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
				perror("shmat");
				exit(1);
			}
			/* Заблокируем разделяемую память */
			if((semop(semid, &lock_res, 1)) == -1){
				fprintf(stderr, "Lock failed\n");
				exit(1);
			} else{
				printf("Semaphore resources decremented by one (locked) \n");
			}
			printf("Количесвто мёда %d \n", (*shm) );
			if ( (*shm) < EAT_HONEY) miskaDead = TRUE;
			if (miskaDead == TRUE) printf("Медведь сдох\n");
			else
			(*shm) = (*shm) - EAT_HONEY;
		}
		printf("Медведь съел %d единиц мёда\n",EAT_HONEY);
		//fflush(stdout);
		printf("Количесвто мёда %d \n", (*shm) );
		/* Освободим разделяемую память */
		if((semop(semid, &rel_res, 1)) == -1){
			fprintf(stderr, "Unlock failed\n");
			exit(1);
		} else{
			printf("Semaphore resources incremented by one (unlocked) \n");
		}
	}
	if (shmdt(shm) < 0) {
		printf("Ошибка отключения\n");
		exit(1);
	}

	return 0;
}

int main(int argc, char const *argv[])
{
	int bees,pid[MAX_BEES],status,stat;

	if ((key = ftok(".", 'S')) < 0) {
			printf("Невозможно получить ключ\n");
			exit(1);
		}
	/* Создадим семафор - для синхронизации работы с разделяемой памятью.*/
	semid = semget(key, 1, 0666 | IPC_CREAT);

	/* Установить в семафоре № 0 (Контроллер ресурса) значение "1" */
	arg.val = 1;
	semctl(semid, 0, SETVAL, arg);

	/* Область разделяемой памяти */
	if ((shmid = shmget(key, sizeof(int), IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		exit(1);
	}



	printf("Введите количесвто пчёл \n");
	scanf("%d",&bees);

	for (int i = 0; i < bees + MISKA; ++i)
	{
		pid[i] = fork();
		srand(getpid());
		if (-1 == pid[i] ){
			perror("fork");
			exit(1);
		} else if (0 == pid[i]){
			if (i == bees){
				printf("Процесс медведь\n");
				exit(mishkaEat());
			} else
			{
				printf("Процесс пчела %d\n", i);
				exit(mineHoney(i));
			}
		}
	}
	printf("PARENT: Это процесс-родитель!\n");
    for (int i = 0; i < bees + MISKA; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
        }
    }


    printf("done\n");
    return 0;
}
