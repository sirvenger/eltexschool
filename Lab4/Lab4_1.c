#include <stdio.h>
#include <stdlib.h>

//void write_char(char ch, FILE *fp);
//1
//Удалить из текста заданный символ 
//Параметры командной строки:
//	1. Имя входного файла 
//	2. Заданный символ

int main(int argc, char const *argv[])
{
	FILE *file,*fp;
	char ch;
	
	if (argc < 2 ){
		fprintf(stderr, "Мало аргументов\n");
		exit(1);
	}

	if ((file=fopen(argv[1],"r")) == NULL)
	{
		printf("Невозможно открыть файл\n");
		exit(1);
	}
	
	if ((fp=fopen(argv[2],"w")) == NULL){
		printf("Невозможно открыть файл\n");
		exit(1);
	}

	while ((ch=fgetc(file)) != EOF ){
		if (ch != argv[3][0]) fputc(ch,fp);
		//write_char(ch,fp);	
	}
	

	return 0;
}
