// getopt и getopt_long 
// из библиотеки unistd.h и  с версии C99 getopt.h.
//Таблица 3
// Вариант 6
//Оставить строки, не содержащие цифры 
//Параметры командной строки:
//	1. Имя входного файла 
//	2. Количество обрабатываемых строк 





#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LEN 100
//void write_char(char ch, FILE *fp);
void write_string(char *str, FILE *fp);

int main(int argc, char const *argv[])
{
	FILE *file,*fp;
	char ch[MAX_LEN],str[20]="";
	int count = 0;

	if (argc < 2 ){
		fprintf(stderr, "Мало аргументов\n");
		exit(1);
	}

	count = atoi(argv[2]);
	//printf("%d\n",count );
	strncpy(str,argv[1],strlen(argv[1])-4);
	strcat(str,".out");
	
	if ((file=fopen(argv[1],"r")) == NULL)
	{
		printf("Невозможно открыть файл\n");
		exit(1);
	}


	if ((fp=fopen(str,"w")) == NULL){
		printf("Невозможно открыть файл\n");
		exit(1);
	}
	
	for (int i = 0; i < count; ++i)
	{
		fgets(ch,MAX_LEN,file);/* code */
		write_string(ch,fp);	
	}
	return 0;
}

void write_string(char *str, FILE *fp){
  while(*str){ 
	if(!ferror(fp)){
		char ch = *str++;
		if (ch < 48 || ch > 57) 
		fputc(ch, fp);
		
	}	
  }	
}


