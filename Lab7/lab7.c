// 5. Винни-Пух и пчелы. Заданное количество пчел добывают 
// мед равными порциями, задерживаясь в пути на случайное 
// время. Винни-Пух потребляет мед порциями заданной 
// величины за заданное время и столько же времени может 
// прожить без питания. 
// Работа каждой пчелы реализуется в порожденном процессе.

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/file.h>
#include <wait.h>
#include <stdbool.h>
#define GET_HONEY 10
#define EAT_HONEY 10 
#define EAT_TIME 2
#define MAX_BEES 100
#define MISKA 1
#define MAX_HONEY 10000
#define FALSE 0
#define TRUE 1


int mineHoney(int bee){
	FILE *fp;
	int honey;
	while (honey <= MAX_HONEY){
		sleep(rand()%4);
		fp = fopen("Ulei","r");
		fscanf(fp,"%d",&honey);
		honey += GET_HONEY;
		fclose(fp);
		fp = fopen("Ulei","w");
		fprintf(fp,"%d",honey);
		printf("Пчела сотворила мёд в количесте %d единиц \n", GET_HONEY );
		fclose(fp);
	}
	return bee;
}

int mishkaEat(){
	FILE *fp;
	int honey;
	int miskaDead = FALSE;
	while (miskaDead == FALSE){
		sleep(EAT_TIME);
		fp = fopen("Ulei","r");
		fscanf(fp,"%d",&honey);
		if ( honey >= EAT_HONEY) honey -= EAT_HONEY; 
		else
		{
			fclose(fp);
			fp = fopen("Ulei","r");
			fscanf(fp,"%d",&honey);
			sleep(EAT_TIME);
			if ( honey < EAT_HONEY) miskaDead = TRUE;
			printf("Медведь сдох\n");
		}
		printf("Медведь съел %d единиц мёда\n",EAT_HONEY);	
		fclose(fp);
		fp = fopen("Ulei","w");
		fprintf(fp,"%d",honey);
		fclose(fp);
	}
	return 0;
}

int main(int argc, char const *argv[])
{
	int bees,pid[MAX_BEES],status,stat,readfd,writefd;
	
	if (argc < 2){
		printf("Не введино количество пчёл\n");
		exit(-1);
	}
	bees = atoi(argv[1]);
	printf("ПЧЕЛЫ СУКА %d\n",bees );
	pipe(fd);
	for (int i = 0; i < bees + MISKA; ++i)
	{
		pid[i] = fork();
		srand(getpid());
		if (-1 == pid[i] ){
			perror("fork");
			exit(1);
		} else if (0 == pid[i]){
			if (i == bees){
				printf("Процесс медведь\n");
				exit(mishkaEat());
			} else
			{
				printf("Процесс пчела %d\n", i);
				exit(mineHoney(i));
			}
		} 
	}
	printf("PARENT: Это процесс-родитель!\n");
    for (int i = 0; i < bees + MISKA; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
        }	
    }
	return 0;
}
