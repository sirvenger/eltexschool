// 5. Винни-Пух и пчелы. Заданное количество пчел добывают 
// мед равными порциями, задерживаясь в пути на случайное 
// время. Винни-Пух потребляет мед порциями заданной 
// величины за заданное время и столько же времени может 
// прожить без питания. 
// Работа каждой пчелы реализуется в порожденном процессе.

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/file.h>
#include <wait.h>
#include <stdbool.h>
#define FIFO1 "Ulei"
#define GET_HONEY 10
#define EAT_HONEY 10 
#define EAT_TIME 2
#define MAX_BEES 100
#define MISKA 1
#define MAX_HONEY 1000
#define FALSE 0
#define TRUE 1
#define MAX_BUF 8 

char* itoa(int i, char b[]){
    char const digit[] = "0123456789";
    char* p = b;
    if(i<0){
        *p++ = '-';
        i *= -1;
    }
    int shifter = i;
    do{ //Move to where representation ends
        ++p;
        shifter = shifter/10;
    }while(shifter);
    *p = '\0';
    do{ //Move back, inserting digits as u go
        *--p = digit[i%10];
        i = i/10;
    }while(i);
    return b;
}

int mineHoney(int bee){
	int honey = 0,readfd,writefd;
	char buff[MAX_BUF];
	char *buf;
	size_t n,len;
	memset(buff, '\0', MAX_BUF);
	while (honey <= MAX_HONEY){
		sleep(rand()%4);
		readfd = open (FIFO1,O_RDONLY,0);
		if ((n = read(readfd,buff,MAX_BUF) > 0)) {
			honey = atoi(buff);
			honey += GET_HONEY;
			close(readfd);		
		printf("Пчела сотворила мёд в количесте %d единиц \n", GET_HONEY );
		printf("Общее количество мёда %d\n",honey);
		writefd = open (FIFO1,O_WRONLY,0);
		buf = itoa(honey,buff);
		len = strlen(buf);
		write(writefd,buf,len);
		close(writefd);
		} else close(readfd);
	}
	return bee;
}

int mishkaEat(){
	int honey,readfd,writefd;
	size_t n,len;
	char buff[MAX_BUF];
	char *buf;
	int miskaDead = FALSE;
	memset(buff, '\0', MAX_BUF);
	while (miskaDead == FALSE){
		sleep(EAT_TIME);
		readfd = open (FIFO1,O_RDONLY,0);
		if ((n = read(readfd,buff,MAX_BUF) > 0)) honey = atoi(buff);
		if ( honey >= EAT_HONEY) honey -= EAT_HONEY; 
		else
		{
			sleep(EAT_TIME/2);
			readfd = open (FIFO1,O_RDONLY,0);
			if ((n = read(readfd,buff,MAX_BUF) > 0)) 
			{
				honey = atoi(buff);
				if ( honey < EAT_HONEY) {	
				miskaDead = TRUE;
				printf("Медведь сдох\n");
				} else honey -= EAT_HONEY;
			} else printf("Пусто\n");
		}
		if (miskaDead == FALSE ) printf("Медведь съел %d единиц мёда\n",EAT_HONEY);	
		printf("Осталось мёда %d\n", honey);
		close(readfd);
		writefd = open (FIFO1,O_WRONLY,0);
		buf = itoa(honey,buff);
		len = strlen(buf);
		write(writefd,buf,len);
		close(writefd);
	}
	return 0;
}

int main(int argc, char const *argv[])
{
	int bees,pid[MAX_BEES],status,stat;
	if (argc < 2){
		printf("Не введино количество пчёл\n");
		exit(-1);
	}
	mkfifo(FIFO1,0666);
	bees = atoi(argv[1]);
	printf("количество пчёл %d\n",bees );
	for (int i = 0; i < bees + MISKA; ++i)
	{
		pid[i] = fork();
		srand(getpid());
		if (-1 == pid[i] ){
			perror("fork");
			exit(1);
		} else if (0 == pid[i]){
			if (i == bees){
				printf("Процесс медведь\n");
				exit(mishkaEat());
			} else
			{
				printf("Процесс пчела %d\n", i);
				exit(mineHoney(i));
			}
		} 
	}
	printf("PARENT: Это процесс-родитель!\n");
    for (int i = 0; i < bees + MISKA; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(stat));
        }	
    }
	return 0;
}
